rc_expiredstatus
================

Changes expired entries to have the status of "Expired"

This is proof of concept code, and is used at your own risk.

It currently runs the update on control panel login, but a future enhancement would be to run it on entry update also.
